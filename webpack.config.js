const path = require("path");
const CopyWebpackPlugin = require("copy-webpack-plugin");

const destination = path.join(__dirname, "dist");

module.exports = {
  entry: "./app/index.js",
  output: {
    path: destination,
    filename: "bundle.js"
  },
  module: {
    loaders: [
      { test: /\.css$/, loader: "style-loader!css-loader"}
    ]
  },
  plugins: [
    new CopyWebpackPlugin([
      { context: "app", from: "**/*.html", to: destination}
    ])
  ],
  devtool: "source-map"
};
