"use strict";

require("simplemde/dist/simplemde.min.css");
require("./w3.css");

var SimpleMDE = require("simplemde");
var _ = require("lodash");
var angular = require("angular");
require("restangular");

angular.module("todo.site", ["restangular"]);

angular.module("todo.site")
  .controller("TodoController", ["Restangular", function (Restangular) {

    var vm = this;

    vm.save = save;
    vm.load = load;

    var simplemde = new SimpleMDE();

    function save() {
      Restangular.all("todo")
        .post({text: simplemde.value()});
    }

    function load() {
      Restangular.one("todo").get()
        .then(Restangular.stripRestangular)
        .then(loadData);
    }

    function loadData(data) {
      simplemde.value(data.text);
    }

  }]);
