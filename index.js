"use strict";

const _ = require("lodash");
const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const fs = require("fs-promise");

const app = express();

const defaultConfiguration = require("./config.default.json");

let configuration = {};

let todoPath;

app.use(bodyParser.json());

app.get("/todo", function (req, res) {
  fs.readJson(todoPath)
    .then(function (data) {
      console.log("Sending: " + JSON.stringify(data));
      res.json(data);
    })
    .catch(function (err) {
      console.error(err);
      res.status(400).end();
    });
});

app.post("/todo", function (req, res) {
  console.log(req.body);
  fs.writeJson(todoPath, req.body)
    .then(function () {
      res.status(200).end();
    })
    .catch(function (err) {
      console.error(err);
      res.status(400).end();
    });
});

app.use(express.static(__dirname + "/dist"));

fs.readJson("config.json")
  .then(function (config) {
    configuration = config;
  })
  .catch(function (err) {
    console.log("Defaulting to the default configuration");
  })
  .then(function () {
    _.defaults(configuration, defaultConfiguration);
    todoPath = path.join(configuration.storage, "todo");
    return fs.ensureFile(todoPath);
  })
  .then(function () {
    console.log("Storage directory ready: " + configuration.storage);
    console.log("Starting server...");
    let server = app.listen(configuration.port, function () {
      let address = server.address().address;
      let port = server.address().port;
      console.log("Server started at: " + address + ":" + port);
    });
  })
  .catch (function (err) {
    console.error("Unable to create directory: ");
    console.error(err);
    console.err("Terminating");
  });
