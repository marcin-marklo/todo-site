This has been developed and tested using `node.js` version `5.0`.

# Setup
1. Install global tools
   `$ npm install -g webpack`
1. Install dependencies
   `$ npm install`
1. Generate static content
   `$ webpack`
1. Run the server
   `node index.js`

# Server configuration
The server has very limited configuration options. Create a file named `config.json` in the project's root directory. See the `config.default.json` file for defaults:

- `port` - the port on which the server will listen
- `storage` - directory where TODOs will be stored